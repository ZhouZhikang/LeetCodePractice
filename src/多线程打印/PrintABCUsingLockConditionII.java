package 多线程打印;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class PrintABCUsingLockConditionII {
    private int times;
    private volatile int count=1;
    private static ReentrantLock lock = new ReentrantLock();
    private static Condition c1=lock.newCondition();
    private static Condition c2=lock.newCondition();
    private static Condition c3=lock.newCondition();

    public PrintABCUsingLockConditionII(int times){
        this.times=times;
    }

    public static void main(String[] args) {
        PrintABCUsingLockConditionII print = new PrintABCUsingLockConditionII(10);
        new Thread(()->{
            for(int i=0;i< print.times;i++) {
                print.printLetter("A",1,5, c1, c2);
            }
        }).start();
        new Thread(()->{
            for(int i=0;i< print.times;i++) {
                print.printLetter("B",6,15, c2, c3);
            }
        }).start();
        new Thread(()->{
            for(int i=0;i< print.times;i++) {
                print.printLetter("C",16,30, c3, c1);
            }
        }).start();
    }

    private void printLetter(String letter,int start,int end,Condition current,Condition next){
            lock.lock();
            try {
                while (start<=count&&count<=end) {
                    System.out.print(letter);
                    count++;
                }
                if(letter.equals("C")){
                    count=1;
                    System.out.println();
                }
                next.signal();
                current.await();
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                lock.unlock();
            }
    }
}
