package 多线程打印;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class PrintABCUsingLockCondition {
    int times;
    int count;
    private static ReentrantLock reentrantLock = new ReentrantLock();
    private static Condition c1 = reentrantLock.newCondition();
    private static  Condition c2 = reentrantLock.newCondition();
    private static Condition c3 = reentrantLock.newCondition();

    public static void main(String[] args) {
        PrintABCUsingLockCondition printABCUsingLockCondition = new PrintABCUsingLockCondition(2);
        new Thread(()->{
           printABCUsingLockCondition.print("A",0,c1,c2);
        }).start();
        new Thread(()->{
            printABCUsingLockCondition.print("B",1,c2,c3);
        }).start();
        new Thread(()->{
            printABCUsingLockCondition.print("C",2,c3,c1);
        }).start();
    }

    public PrintABCUsingLockCondition(int times){
        this.times=times;
    }


    private void print(String name,int target,Condition current,Condition next){
        for(int i=0;i<times;i++){
            reentrantLock.lock();
            try {
                while (count % 3 != target) {
                    current.await();
                }
                count++;
                System.out.println(name);
                next.signal();
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                reentrantLock.unlock();
            }
        }
    }
}
