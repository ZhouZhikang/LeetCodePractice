package 排序;

public class QuickSort {
    public static void main(String[] args) {
        int[] nums = {3, 2, 1, 5, 6, 4};
        quickSort2(nums, 0, nums.length);
        System.out.println(nums);
    }

    private static void quickSort(int[] nums, int l, int r) {
        if (l + 1 >= r) {
            return;
        }
        int first = l, last = r - 1, key = nums[first];
        while (first < last) {
            while (first < last && nums[last] >= key) {
                --last;
            }
            nums[first] = nums[last];
            while (first < last && nums[first] <= key) {
                ++first;
            }
            nums[last] = nums[first];
        }
        nums[first] = key;
        quickSort(nums, l, first);
        quickSort(nums, first + 1, r);
    }

    private static void quickSort2(int[] nums, int l, int r) {
        if (l + 1 >= r) {
            return;
        }
        int first = l, last = r - 1, key =nums[first];
        while (first < last) {
            while (first < last&&nums[last] >= key) {
                last--;
            }
            nums[first] = nums[last];
            while (first < last&&nums[first] <= key) {
                first++;
            }
            nums[last] = nums[first];
        }
        nums[first] = key;
        quickSort2(nums,l,first);
        quickSort2(nums,last+1,r);
    }

}
