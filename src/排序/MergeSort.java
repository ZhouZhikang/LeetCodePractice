package 排序;

public class MergeSort {
    public static void main(String[] args) {
        int[] nums = {9,8,7,6,5,4,3,2,1};
        mergeSort(nums,0,nums.length,new int[9]);
        System.out.println(nums);
    }

    private static void mergeSort(int[] nums,int l,int r,int[] temp){
        if (l + 1 >= r) {
            return;
        }
        // divide
        int m = (l+r) / 2;
        mergeSort(nums, l, m, temp);
        mergeSort(nums, m, r, temp);
        // conquer
        int p = l, q = m, i = l;
        while (p < m || q < r) {
            if (q >= r || (p < m && nums[p] <= nums[q])) {
                temp[i++] = nums[p++];
            } else {
                temp[i++] = nums[q++];
            }
        }
        //排序好的数组反写给nums
        for (i = l; i < r; ++i) {
            nums[i] = temp[i];
        }
    }
}
