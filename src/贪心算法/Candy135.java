package 贪心算法;

public class Candy135 {
    public static int candy(int[] ratings) {
        int count = 0;
        int[] num = new int[ratings.length];
        for(int i =0;i<ratings.length;i++){
            if(i+1<ratings.length&&ratings[i]<ratings[i+1]){
                num[i+1]=num[i]+1;
            }
        }
        for(int i=ratings.length-1;i>0;i--){
            if(i-1>=0&&ratings[i]<ratings[i-1]&&num[i]>=num[i-1]){
                num[i-1]=num[i]+1;
            }
        }
        for(int i=0;i<num.length;i++){
            count+=num[i];
        }
        return count+ratings.length;
    }

    public static void main(String[] args) {
        int[] candy = {1,2,2};
        System.out.println(candy(candy));
    }
}
