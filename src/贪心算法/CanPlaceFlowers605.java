package 贪心算法;

public class CanPlaceFlowers605 {
    public static void main(String[] args) {
        int[] flowerbed = {0};
        int n = 2;
        System.out.println(canPlaceFlowers(flowerbed,n));
    }

    private static boolean canPlaceFlowers(int[] flowerbed, int n) {
        int count =0;
        for(int i=0;i<flowerbed.length;i++){
            if(i==0){
                if(flowerbed.length>1) {
                    if (flowerbed[i] == 0 && flowerbed[i + 1] == 0) {
                        count++;
                        flowerbed[i] = 1;
                    }
                }else{
                    if(flowerbed[i] == 0){
                        count++;
                        flowerbed[i] = 1;
                    }
                }
            }else if(i==flowerbed.length-1){
                if(flowerbed.length>1) {
                    if (flowerbed[i] == 0 && flowerbed[i - 1] == 0) {
                        count++;
                        flowerbed[i] = 1;
                    }
                }else{
                    if(flowerbed[i] == 0){
                        count++;
                        flowerbed[i] = 1;
                    }
                }
            }else if(flowerbed[i-1]==0&&flowerbed[i]==0&&flowerbed[i+1]==0){
                flowerbed[i]=1;
                count++;
            }
        }
        if(n<=count){
            return true;
        }else{
            return false;
        }
    }
}
