package 贪心算法;

import java.util.Arrays;

public class AssignCookies455 {
    public static int findContentChildren(int[] g, int[] s) {
        Arrays.sort(g);
        Arrays.sort(s);
        int count = 0;
        for (int i = 0; i < g.length; i++) {
            for (int j = 0; j < s.length; j++) {
                if (s[j] >= g[i]) {
                    count++;
                    s[j] = 0;
                    break;
                }
            }
        }
        System.out.println(count);
        return 0;
    }

    public static void main(String[] args) {
        int[] g = {1, 2};
        int[] s = {1, 2,3};

        findContentChildren(g, s);
    }
}
