package 双指针;

public class TwoSumII_InputArrayIsSorted167 {
    public static void main(String[] args) {
        int[] numbers = {2,7,11,15};
        System.out.println(twoSum(numbers,9));
    }

    //暴力循环遍历也可以解
    public static int[] twoSum(int[] numbers, int target) {
        for(int i=0;i<numbers.length-1;i++){
            for(int j=i+1;j<numbers.length;j++){
                if(numbers[i]+numbers[j]==target){
                    int[] result = new int[2];
                    result[0]=i+1;
                    result[1]=j+1;
                    return result;
                }
            }
        }
        return null;
    }
}
