package 双指针;

public class MergeSortedArray88 {
    public static void main(String[] args) {
        int[] nums1 = {-1,0,0,3,3,3,0,0,0};
        int m = 6;
        int[] nums2 = {1,2,2};
        int n = 3;
        merge(nums1, m, nums2, n);
    }

    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int[] result = new int[m + n];
        int n1 = 0;
        int n2 = 0;
        if (nums2.length != 0) {
            for (int i = 0; i < result.length; i++) {
                if(n2<nums2.length) {
                    if (nums1[n1] < nums2[n2] && n1<m) {
                        result[i] = nums1[n1];
                        n1++;
                    } else {
                        result[i] = nums2[n2];
                        n2++;
                    }
                }else{
                    result[i]=nums1[n1];
                    n1++;
                }
            }
            for (int i = 0; i < result.length; i++) {
                nums1[i] = result[i];
            }
        }
        System.out.println(nums1);
    }
}
