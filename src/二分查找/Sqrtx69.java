package 二分查找;

public class Sqrtx69 {
    public static void main(String[] args) {
        System.out.println(mySqrt(2147395599));
    }

    public static int mySqrt(int x) {
        int ans=0,l=0,r=x;
        while(l<=r){
            int mid=(l+r)/2;
            if((long)mid*mid<=x){
                ans=mid;
                l=mid+1;
            }else{
                r=mid-1;
            }
        }
        return ans;
    }
}
