package 二分查找;

public class FindMinimumInRotatedSortedArrayII154 {
    public static void main(String[] args) {
        int[] nums = {1,3,3};
        System.out.println(findMin(nums));
    }

    public static int findMin(int[] nums) {
        int l=0,r=nums.length-1;
        while(l<r){
            int mid = l+(r-l)/2;
            if(nums[mid]>nums[r]){
                l=mid+1;
            }else if(nums[mid]<nums[r]){
                r=mid;
            }else{
                r--;
            }
        }
        return nums[l];
    }
}
