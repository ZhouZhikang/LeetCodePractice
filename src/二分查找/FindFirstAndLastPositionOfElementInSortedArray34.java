package 二分查找;

import java.util.Arrays;

public class FindFirstAndLastPositionOfElementInSortedArray34 {
    public static void main(String[] args) {
        int[] nums ={1,2,3};
        Arrays.sort(nums);
        searchRange(nums,3);
    }

    public static int[] searchRange(int[] nums, int target) {
        int l=-1,r=-1;
        for(int i=0;i<nums.length;i++){
            if(nums[i]==target&&l<0){
                l=i;
                r=i;
            }else if(nums[i]==target){
                r=i;
            }
        }
        int[] re={l,r};
        return re;
    }
}
